FROM node:15 AS base

WORKDIR  /
COPY package.json .
COPY package-lock.json .

RUN npm install

COPY public/ public/
COPY src/ src/

RUN npm run build

FROM nginx:alpine as prod
COPY --from=base /build /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]



